package com.company;

import java.io.*;
import java.util.*;

public class Huffman {
    static final int BYTE_SIZE = 8;
    static final int ALPHABET_MAX_SIZE = 256;

    char[] alphabet;
    int[] freq;
    Code[] codes;
    String text;

    static HuffmanCoder codeFrom(String fileName) throws IOException {
        StringBuilder text = new StringBuilder();
        FileReader fr = new FileReader(fileName);
        int c;
        while((c = fr.read()) != -1)
            text.append((char)c);
        fr.close();
        return new HuffmanCoder(text.toString());
    }
    public static HuffmanEncoder from(String fileName) throws IOException {
        return new HuffmanEncoder(fileName);
    }

    TreeNode countTree() {
        PriorityQueue<TreeNode> q = new PriorityQueue<>();
        for (char c : alphabet) {
            q.add(new TreeNode(Character.toString(c), freq[c]));
        }

        while (q.size() != 1) {
            TreeNode t1 = q.poll();
            TreeNode t2 = q.poll();

            q.add(new TreeNode(t1.symbols+t2.symbols,
                    t1.freq+t2.freq, t1, t2));
        }
        return q.poll();
    }
    void countCodes(TreeNode head) {
        int j = 0;
        final int h = head.getHeight();
        final int BYTES_4_LONGEST_CODE = (h + BYTE_SIZE - 1) / BYTE_SIZE;

        byte[] bCode = new byte[BYTES_4_LONGEST_CODE * BYTE_SIZE];

        codes = new Code[ALPHABET_MAX_SIZE];
        Logger.logn("counted codes: ");
        for (char c: alphabet) {
            int i = 0;
            TreeNode buf = head;
            while (buf != null) {
                if (buf.left != null && buf.left.symbols.indexOf(c) != -1) {
                    bCode[i++] = 0;
                    buf = buf.left;
                } else if (buf.right != null) {
                    bCode[i++] = 1;
                    buf = buf.right;
                } else {
                    buf = buf.right;
                }
            }

            StringBuilder sCode = new StringBuilder(c + ": ");
            for (int k = 0; k < i; k++)
                sCode.append(bCode[k]);
            Logger.logn("\t" + sCode.toString());

            codes[c] = new Code(BYTES_4_LONGEST_CODE);
            codes[c].setBits(bCode, i);
        }
    }


    static class HuffmanCoder extends Huffman {
        HuffmanCoder(String text) {
            this.text = text;

            freq = new int[ALPHABET_MAX_SIZE];
            countFreqAlphabet();
            TreeNode head = countTree();
            countCodes(head);
        }
        private void countFreqAlphabet() {
            int alphabetSize = 0;

            for (char c : text.toCharArray()) {
                freq[c]++;
                //face a new symbol
                if (freq[c] == 1)
                    alphabetSize++;
            }
            alphabet = new char[alphabetSize];
            for (int i = 0, j = 0; i < freq.length; i++) {
                if (freq[i] != 0) {
                    alphabet[j++] = (char) i;
                }
            }
        }
        public void to(String fileName) throws IOException {
            FileOutputStream fos = new FileOutputStream(new File(fileName));
            DataOutputStream dos = new DataOutputStream(fos);

            byte[] bytes = new byte[text.length()];

            int fullness = 0;
            byte curr = 0;
            int textPos = 0;
            int bytePos = 0;
            int cursor = 0;

            Logger.logn("input text: " + text);
            char c = text.charAt(textPos++);
            Code code = codes[c];
            Logger.log("was wrote: ");
            while (textPos < text.length() || cursor != code.size) {
                for (; cursor < code.size && fullness < BYTE_SIZE; cursor++) {
                    curr <<= 1;
                    curr = (byte) (curr | (code.getBit(cursor)));
                    fullness++;
                }
                if (fullness == BYTE_SIZE) {
                    fullness = 0;
                    bytes[bytePos++] = curr;

                    Logger.log(String.format("%8s", Integer.toBinaryString(curr & 0xFF)).replace(' ', '0'));

                    curr = 0;
                } else if ((textPos == text.length() && cursor == code.size)) {
                    curr = (byte)(curr << (BYTE_SIZE-fullness));
                    bytes[bytePos++] = curr;

                    Logger.log(String.format("%8s", Integer.toBinaryString(curr & 0xFF)).replace(' ', '0'));

                    break;
                } else {
                    if (textPos < text.length()) {
                        c = text.charAt(textPos++);
                        cursor = 0;
                    }
                    code = codes[c];
                }
            }
//            {
//                //code last symbol
//                bytes[bytePos++] = curr;
//                Logger.log(String.format("%8s", Integer.toBinaryString(curr & 0xFF)).replace(' ', '0'));
//            }
            Logger.logn("");
            for (int f : freq) {
                dos.writeInt(f);
            }
            dos.writeInt(text.length());
            dos.writeInt(bytePos);
            dos.write(bytes, 0, bytePos);

            dos.close();
            fos.close();
        }
    }
    static class HuffmanEncoder extends Huffman {
        int textLength;
        byte[] bytes;
        TreeNode head;

        HuffmanEncoder(String fileName) throws IOException {
            FileInputStream fis = new FileInputStream(new File(fileName));
            DataInputStream dis = new DataInputStream(fis);

            int byteCount;
            int alphabetSize = 0;
            freq = new int[ALPHABET_MAX_SIZE];
            for (int i = 0; i < freq.length; i++) {
                freq[i] = dis.readInt();
                if(freq[i] != 0) {
                    alphabetSize++;
                }
            }
            textLength = dis.readInt();
            alphabet = new char[alphabetSize];
            for (int i = 0, j = 0; i < freq.length; i++) {
                if (freq[i] != 0) {
                    alphabet[j++] = (char)i;
                }
            }
            byteCount = dis.readInt();
            bytes = new byte[byteCount];
            dis.read(bytes, 0, byteCount);
            Logger.log("was read: ");
            for (byte b : bytes) {
                Logger.log(String.format("%8s", Integer.
                        toBinaryString(b & 0xFF)).replace(' ', '0'));
            } Logger.logn("");
            head = countTree();

            dis.close();
            fis.close();
        }


        String encode() {
            StringBuilder text = new StringBuilder();
            int i = 0;

            int cursor = 0;
            int curBit = 0;
            byte curByte;
            TreeNode buf = head;
            curByte = bytes[i++];
            Logger.log("was process: ");
            while (text.length() != textLength) {

                while(buf.symbols.length() != 1) {
                    if (cursor == BYTE_SIZE) {
                        cursor = 0;
                        if (i < bytes.length)
                            curByte = bytes[i++];
                    }
                    curBit = ((curByte & 0xA0) >> (BYTE_SIZE-1));
                    Logger.log(Integer.toString(curBit));
                    curByte <<= 1;
                    cursor++;
                    if (curBit == 0) {
                        buf = buf.left;
                    } else {
                        buf = buf.right;
                    }

                }
                text.append(buf.symbols);
                buf = head;

            }

            return text.toString();
        }
    }




    static final class Code {
        int size;
        byte[] data;

        Code(int capacity) {
            data = new byte[capacity];
        }

        void setBit(byte value, int pos) {
            int i = pos / BYTE_SIZE;
            int bitNumber = pos - i * BYTE_SIZE;
            data[i] = (byte)(value == 1 ?
                    (data[i] | (1 << bitNumber)) :         //set 1
                    (data[i] & (0xff ^ (1 << bitNumber))));//set 0
        }

        int getBit(int pos) {
            int i = pos / BYTE_SIZE;
            int bitNumber = size - pos - 1;
            return (data[i] & (1 << bitNumber)) >> bitNumber;
        }

        void setBits(Iterable<Byte> code) {
            for (Byte bit : code) {
                setBit(bit, size);
                size++;
            }
        }

        void setBits(byte[] code, int len) {
            for (len--; len >= 0; len--) {
                setBit(code[len], size);
                size++;
            }
        }
    }
    static final class TreeNode implements Comparable{
        TreeNode left;
        TreeNode right;

        String symbols;
        int freq;

        TreeNode(String symbols, int freq) {
            this.symbols = symbols;
            this.freq = freq;
            this.left = null;
            this.right = null;
        }

        TreeNode(String symbols, int freq,
                 TreeNode left, TreeNode right) {
            this(symbols, freq);
            this.left = left;
            this.right = right;
        }

        int getHeight() {
            int lh = 0;
            int rh = 0;
            if (this.left != null)
                lh = this.left.getHeight();
            if (this.right != null)
                rh = this.right.getHeight();
            return lh > rh ? lh + 1 : rh + 1;
        }

        @Override
        public int compareTo(Object o) {
            //min-top priority queue
            return (freq - ((TreeNode)o).freq);
        }
    }
}
