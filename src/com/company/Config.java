package com.company;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Config {
    Mode mode;
    String input;
    String output;


    static Config fromConfigFile(String fileName) throws IOException {
        return ConfigParser.parse(fileName);
    }


    static class ConfigParser {

        static Config parse(String fileName) throws IOException {
            Config conf = new Config();
            StringBuilder text = new StringBuilder();
            FileReader fr = new FileReader(fileName);
            int c;
            while((c = fr.read()) != -1)
                if(c != '\r')
                    text.append((char)c);
            fr.close();

            String[] lines = text.toString().split("\n");
            for (String line: lines) {
                String[] param = line.split(": ");
                if (param[0].equals("input")) {
                    conf.input = param[1];
                } else if (param[0].equals("output")) {
                    conf.output = param[1];
                } else if (param[0].equals("mode")) {
                    if (param[1].equals("code")) {
                        conf.mode = Mode.CODE;
                    } else if (param[1].equals("encode")) {
                        conf.mode = Mode.ENCODE;
                    } else {
                        conf.mode = Mode.UNDEFINED;
                    }
                }
            }


            return conf;
        }
    }

    enum Mode {CODE, ENCODE, UNDEFINED};
}
