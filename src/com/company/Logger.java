package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
    private static FileWriter fw;
    public static void init(String fileName) {
        try {
            fw = new FileWriter(new File(fileName));
        } catch (IOException e) {
            System.out.println("problems with logger file");
        }
    }
    public static void logn(String message) {
        try {
            fw.write(message+'\n');
            fw.flush();
        } catch (IOException e) {
            System.out.println("problem with writing a log message");
        }
    }
    public static void log(String message) {
        try {
            fw.write(message);
            fw.flush();
        } catch (IOException e) {
            System.out.println("problem with writing a log message");
        }
    }
    public static void close() {
        try {
            fw.close();
        } catch (IOException e) {
            System.out.println("problem with closing a file");
        }
    }
}
