package com.company;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Logger.init("log.txt");
        Config conf;
        try {
            conf = Config.fromConfigFile(args[0]);
        } catch (IOException e) {
            System.out.println("Problems with config file. Check log file for more information");
            Logger.logn(e.getMessage());
            return;
        }

        try {
            switch (conf.mode) {
                case CODE:
                    Huffman.codeFrom(conf.input).to(conf.output);
                    break;
                case ENCODE:
                    String text = Huffman.from(conf.input).encode();
                    FileWriter fw = new FileWriter(conf.output);
                    fw.write(text);
                    fw.close();
                    break;
                case UNDEFINED:
                    System.out.println("Undefined mode, check config file");
                    break;
            }
        } catch (IOException e) {
            System.out.println("Problems during processing file. Check log file for more information");
            Logger.logn(e.getMessage());
        }
        Logger.close();
    }
}
